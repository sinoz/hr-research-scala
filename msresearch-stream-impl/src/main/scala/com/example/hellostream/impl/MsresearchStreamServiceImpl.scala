package com.example.hellostream.impl

import com.lightbend.lagom.scaladsl.api.ServiceCall
import com.example.hellostream.api.MsresearchStreamService
import com.example.hello.api.MsresearchService

import scala.concurrent.Future

/**
  * Implementation of the MsresearchStreamService.
  */
class MsresearchStreamServiceImpl(msresearchService: MsresearchService) extends MsresearchStreamService {
  def stream = ServiceCall { hellos =>
    Future.successful(hellos.mapAsync(8)(msresearchService.hello(_).invoke()))
  }
}
