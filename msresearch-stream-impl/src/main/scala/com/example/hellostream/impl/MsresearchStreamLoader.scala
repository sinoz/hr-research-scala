package com.example.hellostream.impl

import com.lightbend.lagom.scaladsl.api.ServiceLocator.NoServiceLocator
import com.lightbend.lagom.scaladsl.server._
import com.lightbend.lagom.scaladsl.devmode.LagomDevModeComponents
import play.api.libs.ws.ahc.AhcWSComponents
import com.example.hellostream.api.MsresearchStreamService
import com.example.hello.api.MsresearchService
import com.softwaremill.macwire._

class MsresearchStreamLoader extends LagomApplicationLoader {

  override def load(context: LagomApplicationContext): LagomApplication =
    new MsresearchStreamApplication(context) {
      override def serviceLocator = NoServiceLocator
    }

  override def loadDevMode(context: LagomApplicationContext): LagomApplication =
    new MsresearchStreamApplication(context) with LagomDevModeComponents

  override def describeService = Some(readDescriptor[MsresearchStreamService])
}

abstract class MsresearchStreamApplication(context: LagomApplicationContext)
  extends LagomApplication(context)
    with AhcWSComponents {

  // Bind the service that this server provides
  override lazy val lagomServer = serverFor[MsresearchStreamService](wire[MsresearchStreamServiceImpl])

  // Bind the MsresearchService client
  lazy val msresearchService = serviceClient.implement[MsresearchService]
}
